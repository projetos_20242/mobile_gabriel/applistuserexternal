import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import usersList from "./src/usersList";
import usersDetails from "./src/usersDetails";


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="usersList">
        <Stack.Screen name="usersList" component={usersList}/>
        <Stack.Screen name="usersDetails" component={usersDetails} options={{title:"Informações dos usuários"}}/>
      </Stack.Navigator>

    </NavigationContainer>
  );
}


