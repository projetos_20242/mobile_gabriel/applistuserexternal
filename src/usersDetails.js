import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const usersDetails = ({ route }) => {
  const { task } = route.params;

  return (
    <View>
      <Text>Detalhes dos Usuários</Text>
      <Text>Nome: {task.name}</Text>
      <Text>ID : {task.id}</Text>
      <Text>Username : {task.username}</Text>
      <Text>email : {task.email}</Text>
      <Text>Rua : {task.address.street}</Text>
      <Text>Suíte : {task.address.suite}</Text>
      <Text>Cidade : {task.address.city}</Text>
      <Text>Zipcode : {task.address.zipcode}</Text>
      <Text>Latitude : {task.address.geo.lat}</Text>
      <Text>Longitude : {task.address.geo.lng}</Text>
    </View>
  );
};
export default usersDetails;
