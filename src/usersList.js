import React, { useEffect, useState } from "react";
import { View, Text, FlatList } from "react-native";
import axios from "axios";
import { TouchableOpacity } from "react-native-gesture-handler";

const usersList = ({ navigation }) => {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      setUsers(response.data);
    } catch (error) {}
    console.error(error);
  }
  const taskPress = (task) => {
    navigation.navigate("usersDetails", { task });
  };

  return (
    <View>
      <Text> Lista de Usuários </Text>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <View>
            <TouchableOpacity onPress={() => taskPress(item)}>
              Nome: {item.name}
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  );
};
export default usersList;
